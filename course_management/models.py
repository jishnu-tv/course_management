from flask import session
from course_management import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def get_user(user_id):
    role = session.get('role')
    if role == 'admin':
        return Admin.query.get(int(user_id))
    elif role == 'instructor':
        return Instructor.query.get(int(user_id))
    elif role == 'student':
        return Student.query.get(int(user_id))

applied = db.Table('applied',
    db.Column('student_id' ,db.Integer, db.ForeignKey('student.id')),
    db.Column('course_id' ,db.Integer, db.ForeignKey('courses.id'))
)


class Admin(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(5), default="admin")


class Instructor(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(30), nullable=False)
    lastName = db.Column(db.String(30), nullable=False)
    gender = db.Column(db.String(10), nullable=False)
    dateofBirth = db.Column(db.String(15), nullable=False)
    qualification = db.Column(db.String(120), nullable=False)
    photo = db.Column(db.String(120), default="default-user.jpg")
    title = db.Column(db.String(120), nullable=False)
    about = db.Column(db.String(256), nullable=False)
    phonenumber = db.Column(db.String(10), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(10), default="instructor")
    course = db.relationship('Courses', uselist=True, backref='inst', lazy=True)

class Student(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(50), nullable=False)
    lastName = db.Column(db.String(50), nullable=False)
    gender = db.Column(db.String(120), nullable=False)
    dateofBirth = db.Column(db.String(120), nullable=False)
    qualification = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    phonenumber = db.Column(db.String(10), nullable=False, unique=True)
    photo = db.Column(db.String(120), default="default-user.jpg")
    certificate = db.Column(db.String(120))
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(10), default="student")
    courses = db.relationship('Courses', secondary=applied, backref=db.backref('applied_student', lazy=True, uselist=True))


class Universities(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    universityname = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    phone = db.Column(db.String(120), nullable=False)
    address1 = db.Column(db.String(120), nullable=False)
    address2 = db.Column(db.String(120), nullable=False)
    country = db.Column(db.String(30), nullable=False)
    state = db.Column(db.String(30), nullable=False)
    district = db.Column(db.String(30), nullable=False)
    pincode = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String(200), nullable=False)
    photo = db.Column(db.String(120), default="default-university.jpg")
    course = db.relationship('Courses', backref='univ', lazy=True)

class Courses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    courseName = db.Column(db.String(120), nullable=False)
    university_id = db.Column(db.Integer, db.ForeignKey('universities.id'), nullable=False)
    instructor_id = db.Column(db.Integer, db.ForeignKey('instructor.id'), nullable=False, default=1)
    duration = db.Column(db.String(120), nullable=False)
    courseFee = db.Column(db.String(120), nullable=False)
    coursetype = db.Column(db.String(120), nullable=False)
    eligiblity = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String(250), nullable=False)
    photo = db.Column(db.String(120), default="default-course.jpg")