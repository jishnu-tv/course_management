import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_uploads import configure_uploads, IMAGES, DOCUMENTS, UploadSet


app = Flask(__name__)

cur_w = os.getcwd()
img_path = str(cur_w+"/course_management/static/uploades/images")
file_path = str(cur_w+"/course_management/static/uploades/files")


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///course.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOADED_PHOTOS_DEST'] = img_path
app.config['UPLOADED_FILES_DEST'] = file_path

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
migrate = Migrate(app, db)
images = UploadSet('photos', IMAGES)
files = UploadSet('files', DOCUMENTS)
configure_uploads(app, [images, files])


from course_management import routes
