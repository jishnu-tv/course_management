from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField, BooleanField, TextAreaField, FileField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, Length, EqualTo, ValidationError
from course_management.models import Instructor, Student, Admin

# Login Student Form
class StudentLoginForm(FlaskForm):
    email = EmailField("Email address", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password lenght 8 char")])
    rememberMe = BooleanField("Remember me")


class AdminLoginForm(FlaskForm):
    username = StringField("User Name", validators=[DataRequired(
        "Username is required")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password lenght 8 char")])
    rememberMe = BooleanField("Remember me")

class SearchForm(FlaskForm):
    search = StringField("search universities")

# Login Form
class InstructorLoginForm(FlaskForm):
    email = EmailField("Email address", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password lenght 8 char")])
    rememberMe = BooleanField("Remember me")


# Register Instructor Form
class InstructorRegisterForm(FlaskForm):
    firstName = StringField("First Name", validators=[
                            DataRequired("First Name required")])
    lastName = StringField("Last Name", validators=[
                           DataRequired("Last Name required")])
    gender = SelectField("Gender", choices=[('', 'Gender'), (
        'male', 'Male'), ('Female', 'Female')], validators=[DataRequired("Gender Required")])
    dateofBirth = StringField("Date of Birth", validators=[
                              DataRequired("Date of Birth required")])
    qualification = StringField("Educational Qualification", validators=[
                                DataRequired("Educational Qualification required")])
    photo = FileField('Instructor image')                            
    title = StringField("Job or Work Title", validators=[
        DataRequired("Job or Work Title is required")])
    about = TextAreaField("About", validators=[DataRequired("About required")])
    phonenumber = StringField("Phone number", validators=[
                              DataRequired("Phone number required")])
    email = EmailField("Email address", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password lenght 8 char")])
    confirmPassword = PasswordField("Confirm password", validators=[EqualTo(
        "password", message="Password doesn't match"), DataRequired("Confirm password is required")])

    def validate_email(self, email):
        instructor = Instructor.query.filter_by(email=email.data).first()
        if instructor:
            raise ValidationError("The email address is already in use")

    def validate_phonenumber(self, phonenumber):
        instructor = Instructor.query.filter_by(phonenumber=phonenumber.data).first()
        if instructor:
            raise ValidationError("The phone number is already in use")


# Register Student Form
class StudentRegisterForm(FlaskForm):
    firstName = StringField("First Name", validators=[
                            DataRequired("First Name required")])
    lastName = StringField("Last Name", validators=[
                           DataRequired("Last Name required")])
    gender = SelectField("Gender", choices=[('', 'Gender'), (
        'male', 'Male'), ('Female', 'Female')], validators=[DataRequired("Gender Required")])
    dateofBirth = StringField("Date of Birth", validators=[
                              DataRequired("Date of Birth required")])
    qualification = StringField("Educational Qualification", validators=[
                                DataRequired("Educational Qualification required")])
    email = EmailField("Email address", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    phonenumber = StringField("Phone number", validators=[
                              DataRequired("Phone number required")])
    photo = FileField('Profile image')
    certificate = FileField('Upload Certificate')
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password lenght 8 char")])
    confirmPassword = PasswordField("Confirm password", validators=[EqualTo(
        "password", message="Password doesn't match"), DataRequired("Confirm password is required")])

    def validate_email(self, email):
        student = Student.query.filter_by(email=email.data).first()
        if student:
            raise ValidationError("The email address is already in use")

    def validate_phonenumber(self, phonenumber):
        student  = Student .query.filter_by(phonenumber=phonenumber.data).first()
        if student :
            raise ValidationError("The phone number is already in use")


class AddCourseForm(FlaskForm):
    course_name = StringField("Course Name", validators=[DataRequired("asdf")])
    duration = StringField("Duration", validators=[DataRequired("asdf")])
    course_fee = StringField("Course Fee", validators=[DataRequired("asdf")])
    coursetype = StringField("Course type", validators=[DataRequired("asdf")])
    eligiblity = StringField("Eligiblity", validators=[DataRequired("asdf")])
    description = StringField("Description", validators=[DataRequired("asdf")])
    photo = FileField('Course image')


class AddUniversityForm(FlaskForm):
    universityname = StringField("University Name", validators=[
                                 DataRequired("Email required")])
    email = EmailField("Email", validators=[
                                 DataRequired("Email is required"), Email("Email address is invalid")])
    phone = StringField("Phone number", validators=[
                                 DataRequired("Phone number required")])
    address1 = StringField("Address line 1", validators=[
                                 DataRequired("Address line 1 is required")])
    address2 = StringField("Address line 2")
    country = StringField("Country ", validators=[
                          DataRequired("Country  required")])
    state = StringField("State", validators=[DataRequired("State required")])
    district = StringField("District", validators=[
                           DataRequired("District required")])
    pincode = StringField("Pin code", validators=[
                           DataRequired("Pin code")])
    description = TextAreaField("Description", validators=[
                              DataRequired("Description required")])
    photo = FileField('University image', default="default-university.jpg")
