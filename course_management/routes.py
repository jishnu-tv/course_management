import time
import os
from flask import render_template, request, session, redirect, flash, url_for
from course_management import app, db, bcrypt, images, files
from course_management.forms import StudentLoginForm, InstructorLoginForm, InstructorRegisterForm, StudentRegisterForm, AddCourseForm, AddUniversityForm, AdminLoginForm, SearchForm
from course_management.models import Instructor, Student, Admin, Courses, Universities
from flask_login import login_user, current_user, logout_user


# Home page
@app.route("/", methods=['GET', 'POST'])
def home():
    form = SearchForm()
    if form.validate_on_submit():
        return redirect(url_for('searchUniversities', search=form.search.data))
    return render_template('home.html', title="Course Management System", form=form)


# Universities page
@app.route("/universities", methods=['GET', 'POST'])
def universities():
    university = Universities.query.all()
    return render_template('/universities.html', title="Universities", cls="active", university=university)

# Universities page
@app.route("/universities/<id>")
def universityPage(id):
    uni = Universities.query.filter_by(id=id).first()
    course = ""
    if uni.course:
        course = uni.course
    return render_template('/university-details.html', title=uni.universityname, uni=uni, course=course)

# Universities page
@app.route("/search/<search>", methods=['GET', 'POST'])
def searchUniversities(search):
    val = search
    course = Courses.query.filter(Courses.courseName.like('%' + val + '%'))
    course = course.order_by(Courses.courseName).all()
    print(course)
    return render_template('/search.html', title="Universities", cls="active", sname=val, course=course)

@app.route("/login/admin", methods=['GET', 'POST'])
def adminLogin():
    if current_user.is_authenticated:
        if current_user.role == 'admin':
            return redirect(url_for('admin'))
        else:
            return redirect(url_for('home'))
    form = AdminLoginForm()
    if form.validate_on_submit():
        session.pop('role', None)
        admin = Admin.query.filter_by(user_name=form.username.data).first()
        if admin and bcrypt.check_password_hash(admin.password, form.password.data):
            session['role'] = admin.role
            login_user(admin, remember=form.rememberMe.data)
            return redirect(url_for('admin'))
        else:
            flash(f'Login failed. Username or password is incorrect')
            return redirect(url_for('home'))
    return render_template('/admin/admin-login.html', title="Courses", cls="active", form=form)


# Courses page
@app.route("/courses")
def courses():
    courses = Courses.query.all()
    return render_template('courses.html', title="Courses", cls="active", courses=courses)

# Universities page
@app.route("/courses/<id>")
def coursePage(id):
    course = Courses.query.filter_by(id=id).first()
    if not course:
        return redirect(url_for('courses'))
    else:
        return render_template('/course-details.html', title=course.courseName, course=course )

# About page
@app.route("/about")
def about():
    return render_template('about.html', title="About", cls="active")


# Contact page
@app.route("/contact")
def contact():
    return render_template('contact.html', title="contact", cls="active")


# Login Student page
@app.route("/login/student", methods=['GET', 'POST'])
def loginStudent():
    if current_user.is_authenticated:
        if current_user.role == 'student':
            return redirect(url_for('student'))
        else:
            return redirect(url_for('home'))
    form = StudentLoginForm()
    if form.validate_on_submit():
        session.pop('role', None)
        print("hi")
        student = Student.query.filter_by(email=form.email.data).first()
        if student and bcrypt.check_password_hash(student.password, form.password.data):
            session['role'] = student.role
            login_user(student, remember=form.rememberMe.data)
            return redirect(url_for('student'))
        else:
            flash(f'Login failed. Email or password is incorrect')
            return redirect(url_for('home'))
    return render_template('login-student.html', title="Student login", cls="active", form=form)


@app.route("/apply-course/<course_id>")
def applyCourse(course_id):
    if current_user.is_authenticated:
        if current_user.role == 'student':
            course = Courses.query.filter_by(id=course_id).first()
            course.applied_student.append(current_user)
            db.session.commit()
            flash('You are successfully applied')
            return redirect(url_for('student'))
        else:
            flash(f'Please login as student')
            print(request.referrer)
            return redirect(request.referrer)
    else:
        flash(f'Please login')
        return redirect(url_for('loginStudent'))       
    

# Login Instructor page
@app.route("/login/instructor", methods=['GET', 'POST'])
def loginInstructor():
    if current_user.is_authenticated:
        if current_user.role == 'instructor':
            return redirect(url_for('instructor'))
        else:
            return redirect(url_for('home'))
    form = InstructorLoginForm()
    if form.validate_on_submit():
        session.pop('role', None)
        instructor = Instructor.query.filter_by(email=form.email.data).first()
        if instructor and bcrypt.check_password_hash(instructor.password, form.password.data):
            session['role'] = instructor.role
            login_user(instructor, remember=form.rememberMe.data)
            return redirect(url_for('instructor'))
        else:
            flash(f'Login failed. Email or password is incorrect')
            return redirect(url_for('loginInstructor'))
    return render_template('login-instructor.html', title="Instructor login", cls="active", form=form)


# Register Student page
@app.route("/register/student", methods=['GET', 'POST'])
def registerStudent():
    form = StudentRegisterForm()
    if form.validate_on_submit():
        timestr = time.strftime("%Y%m%d_%H%M%S")

        photo = request.files['photo']
        photo_extension = os.path.splitext(photo.filename)
        photo.filename = str(timestr+"_"+form.email.data+photo_extension[1])
        usr_photo = images.save(form.photo.data)

        files = request.files['certificate']
        files_extension = os.path.splitext(files.filename)
        files.filename = str(timestr+"_"+form.email.data+files_extension[1])
        usr_files = files.save(form.certificate.data)

        

        psw_hash = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        student = Student(firstName=form.firstName.data, lastName=form.lastName.data,
                          gender=form.gender.data, dateofBirth=form.dateofBirth.data,
                          qualification=form.qualification.data, email=form. email.data,
                          phonenumber=form.phonenumber.data, photo=usr_photo, certificate=usr_files, password=psw_hash)
        db.session.add(student)
        db.session.commit()
        flash('You are successfully registered. You can now login')
        return redirect(url_for('loginStudent'))
    return render_template('register-student.html', title="Student register", form=form)




# Register Instructor page
@app.route("/register/instructor", methods=['GET', 'POST'])
def registerInstructor():
    form = InstructorRegisterForm()
    if form.validate_on_submit():
        timestr = time.strftime("%Y%m%d_%H%M%S")
        file = request.files['photo']
        file_ext = os.path.splitext(file.filename)
        file_extension = file_ext[1]
        file.filename = str(timestr+"_"+form.email.data+file_extension)
        photo = images.save(form.photo.data)
        psw_hash = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        instructor = Instructor(firstName=form.firstName.data, lastName=form.lastName.data,
                                gender=form.gender.data, dateofBirth=form.dateofBirth.data,
                                qualification=form.qualification.data, photo=photo, title=form.title.data,
                                about=form.about.data, phonenumber=form.phonenumber.data,
                                email=form.email.data, password=psw_hash)
        db.session.add(instructor)
        db.session.commit()
        flash('You are successfully registered. You can now login')
        return redirect(url_for('loginInstructor'))
    return render_template('register-instructor.html', title="Instructor register", form=form)

# Instructor details page
@app.route("/instructors/<id>")
def instructorPage(id):
    instructor = Instructor.query.filter_by(id=id).first()
    if not instructor:
        return redirect(url_for('courses'))
    else:
        return render_template('/instructor-details.html', title=f'{instructor.firstName} {instructor.lastName}', instructor=instructor )

# Instructor dashboard page
@app.route("/instructor")
def instructor():
    if current_user.is_authenticated and current_user.role == 'instructor':
        return render_template('/instructor/index.html', title="Dashboard | instructor")
    else:
        return redirect(url_for('home'))


# Stdent dashboard page
@app.route("/student")
def student():
    if current_user.is_authenticated and current_user.role == 'student':
        return render_template('/student/index.html', title="Dashboard | Student")
    else:
        return redirect(url_for('home'))


# Admin page
@app.route("/admin")
def admin():
    if current_user.is_authenticated and current_user.role == 'admin':
        course = Courses.query.order_by(Courses.id.desc()).limit(10).all()
        university = Universities.query.order_by(Universities.id.desc()).limit(10).all()
        instructor = Instructor.query.order_by(Instructor.id.desc()).limit(10).all()
        student = Student.query.order_by(Student.id.desc()).limit(10).all()
        return render_template('/admin/index.html', title="Dashboard | admin", cls="active", course=course, university=university, instructor=instructor, student=student)
    else:
        return redirect(url_for('home'))


# Add Courses page
@app.route("/admin/add-course", methods=['GET', 'POST'])
def addCourse():
    form = AddCourseForm()
    selection = request.form.get('university')
    timestr = time.strftime("%Y%m%d_%H%M%S")
    if current_user.is_authenticated and current_user.role == 'admin':
        unv = Universities.query.all()
        if form.validate_on_submit() and request.method == "POST":
            file = request.files['photo']
            file_name, file_extension = os.path.splitext(file.filename)
            file.filename = str(timestr+file_extension)
            photo = images.save(form.photo.data)
            course = Courses(courseName=form.course_name.data, university_id=selection,
                            duration=form.duration.data, courseFee=form.course_fee.data,
                            coursetype=form.coursetype.data,  eligiblity=form.eligiblity.data,
                            description=form.description.data, photo=photo)
            db.session.add(course)
            db.session.commit()
            flash('CourseAdded')
            return redirect(url_for('addCourse'))
    else:
        return redirect(url_for('home'))
    return render_template('/admin/add-course.html', title="Add course", form=form, unv=unv)



# Add Universities page
@app.route("/admin/add-universities", methods=['GET', 'POST'])
def addUniversities():
    form = AddUniversityForm()
    timestr = time.strftime("%Y%m%d_%H%M%S")
    if current_user.is_authenticated and current_user.role == 'admin':
       if form.validate_on_submit() and request.method == "POST":
            file = request.files['photo']
            file_name, file_extension = os.path.splitext(file.filename)
            file.filename = str(timestr+file_extension)
            photo = images.save(form.photo.data)
            university = Universities(universityname=form.universityname.data,
                            email=form.email.data, phone=form.phone.data, 
                            address1=form.address1.data, address2=form.address2.data, 
                            country=form.country.data, state=form.state.data,
                            district=form.district.data, pincode=form.pincode.data,
                            description=form.description.data, photo=photo)
            db.session.add(university)
            db.session.commit()
            flash("University added")
            return redirect(url_for('addUniversities'))
    else:
        return redirect(url_for('home'))
    return render_template('/admin/add-universities.html', title="Universities", cls="active", form=form)



@app.route("/admin/courses")
def adminCourses():
    if current_user.is_authenticated and current_user.role == 'admin':
        course = Courses.query.all()
        return render_template('/admin/manage/course.html', title="Manage courses", course=course)


@app.route("/admin/courses/<id>")
def viewCourse(id):
    course_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        course = Courses.query.filter_by(id=course_id).first()
        inst = Instructor.query.all()
        return render_template('admin/manage/course-details.html', title="Dashboard", course=course, inst=inst)
    else: 
        return redirect(url_for('home'))


# Delete Course
@app.route("/delete-course/<id>")
def deleteCourse(id):
    course_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        course = Courses.query.filter_by(id=course_id).first()
        os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], course.photo))
        db.session.delete(course)
        db.session.commit()
        return redirect(url_for('adminCourses'))
    else: 
        return redirect(url_for('home'))

# View all instructors
@app.route("/admin/instructors")
def adminInstructors():
    if current_user.is_authenticated and current_user.role == 'admin':
        inst = Instructor.query.all()
        return render_template('/admin/manage/instructor.html', title="Manage instructor", inst = inst)
    else:
        return redirect(url_for('home'))

# View instructor
@app.route("/admin/instructors/<id>")
def viewInstructors(id):
    instructor_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
       instructor = Instructor.query.filter_by(id=instructor_id).first()
       return render_template('admin/manage/instructor-details.html', title="Dashboard", instructor=instructor)
    else: 
        return redirect(url_for('home'))        


@app.route("/delete-instructor/<id>")
def deleteInstructors(id):
    print(id)
    # if current_user.is_authenticated and current_user.role == 'admin':
    #     instructor = Instructor.query.filter_by().first()
    #     os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], instructor.photo))
    #     db.session.delete(instructor)
    #     db.session.commit()
    #     return redirect(url_for('adminInstructors'))
    #     return "Deleted"
    return redirect(url_for('home'))

# View all universities
@app.route("/admin/universities")
def adminUniversities():
    if current_user.is_authenticated and current_user.role == 'admin':
        unv = Universities.query.all()
        return render_template('/admin/manage/university.html', title="Manage universities", unv=unv)
    else:
        return redirect(url_for('home'))

# View single university
@app.route("/admin/universities/<id>")
def viewUniversity(id):
    university_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        university = Universities.query.filter_by(id=university_id).first()
        return render_template('admin/manage/university-details.html', title="Dashboard", university=university)
    else: 
        return redirect(url_for('home'))


# Delete University
@app.route("/delete-university/<id>")
def deleteUniversity(id):
    university_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        university = Universities.query.filter_by(id=university_id).first()
        os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], university.photo))
        db.session.delete(university)
        db.session.commit()
        return redirect(url_for('adminUniversities'))
    else: 
        return redirect(url_for('home'))

# View all students
@app.route("/admin/students")
def adminStudents():
    if current_user.is_authenticated and current_user.role == 'admin':
        student = Student.query.all()
        return render_template('/admin/manage/student.html', title="Manage students", student=student)
    else:
        return redirect(url_for('home'))


# Admin view student
@app.route("/admin/students/<id>")
def viewStudent(id):
    student_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        student = Student.query.filter_by(id=student_id).first()
        return render_template('admin/manage/student-details.html', title="Dashboard", student=student)
    else: 
        return redirect(url_for('home'))

# Delete Student
@app.route("/delete-student/<id>")
def deleteStudent(id):
    student_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        student = Student.query.filter_by(id=student_id).first()
        os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], student.photo))
        db.session.delete(student)
        db.session.commit()
        return redirect(url_for('adminStudents'))
    else: 
        return redirect(url_for('home'))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))
