var byRow = $(".container").hasClass(".features");

$(document).ready(function () {
  $(".datepicker").datepicker();

  $(".dropdown-trigger").dropdown({
    coverTrigger: false,
    hover: false,
  });

  $(".feature-col").matchHeight();
  $(".univ-col").matchHeight();

  $("select").formSelect();

  $('.tabs').tabs();

  M.textareaAutoResize($("#textarea1"));

  AOS.init();
});